package com.example.androidexercise1
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.androidexercise1.data.Projects
import kotlinx.android.synthetic.main.card.view.*


class ProjectAdapter : RecyclerView.Adapter<ProjectAdapter.myViewHolder>(){

    private var projectList = emptyList<Projects>()



    class myViewHolder(view: View): RecyclerView.ViewHolder(view){

//        private val title = view.findViewById<TextView>(R.id.description)
//        private val description = view.findViewById<TextView>(R.id.description)
//        private val image = view.findViewById<ImageView>(R.id.imageView2)
//        private val root by lazy {view.rootView}
//
//        fun bindTitle(word: String) {
//            title.text = word
//        }
//
//        fun bindDescription(word: String) {
//            description.text = word
//        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): myViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card, parent, false)

        return myViewHolder(view)

    }

    override fun onBindViewHolder(holder: myViewHolder, position: Int) {
        val currentItem = projectList[position]


        holder.itemView.description.text = currentItem.description.toString()
        holder.itemView.Title.text = currentItem.title.toString()


        holder.itemView.cardLayout.setOnClickListener {
            val action1 = Fragment1Directions.actionFragment1ToFragment2(currentItem)
            holder.itemView.findNavController().navigate(action1)
        }

        holder.itemView.cardLayout.setOnLongClickListener{
            val action2 = Fragment1Directions.actionFragment1ToUpdateFragment2(currentItem)
            holder.itemView.findNavController().navigate(action2)
            true
        }


    }

    override fun getItemCount(): Int {
        return projectList.size
    }

    fun setData(project: List<Projects>){
        this.projectList = project
        notifyDataSetChanged()

    }


}


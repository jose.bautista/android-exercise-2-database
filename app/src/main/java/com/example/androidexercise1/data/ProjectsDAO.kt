package com.example.androidexercise1.data

import androidx.lifecycle.LiveData
import androidx.room.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Dao
interface ProjectsDAO {

    @Query("SELECT * FROM projects ORDER BY id ASC")
    fun getProjects(): LiveData<List<Projects>>

//    @Query("SELECT * FROM projects WHERE id =:id")
//    fun getProject(id: Long): Projects

    @Update
    fun updateProject(project: Projects)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addProject(project: Projects)

    @Query("DELETE FROM projects")
    fun deleteAllProjects()

    @Delete
    fun deleteProject(project: Projects)
}